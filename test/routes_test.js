const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');

chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001';
    // it('GET / endpoint', (done)=>{
    //     chai.request(domain) // accepts a server url to be used on the API unit testing
    //     .get('/')  
    //     .end((error, res)=>{
    //         // assert.equal(res.status,200);
    //         //OR
    //         expect(res.status).to.equal(200);
    //         done(); //signals that the callback is complete // break //
    //     }) // handles the response or an error received from the endpoint
    
	
	// })
	
    it('POST /currency name property is missing', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03
				}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
            done();
        })
    }) 
    it('POST /currency name is not a string', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 2,  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03
				}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
            done();
        })
    }) 
    it('POST /currency name empty field', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: '',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03
				}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency missing ex', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 'US Dollar'  
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency ex not an object', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 'US Dollar',  
				ex: 2
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency ex is an empty object', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 'US Dollar',  
				ex: {}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency alias missing', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				name: 'US Dollar',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency alias not a string', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :5,
				name: 'US Dollar',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency alias is empty', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'',
				name: 'US Dollar',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency alias is already used', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 'US Dollar',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 
    it('POST /currency valid entry not accepted', (done)=>{
        chai.request(domain)
        .post('/currency')  
        .type('json')
        .send({
				alias :'usd',
				name: 'US Dollar',  
				ex: {
					peso: 50.73,
					won: 1187.24,
					yen: 108.63,
					yuan: 7.03
				}
			})  
        .end((err,res) =>{
           expect(res.status).to.equal(400);
		   done();
        })
    }) 







})
