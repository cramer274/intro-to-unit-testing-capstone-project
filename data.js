const currency =[
    
    {
        'alias': 'riyal',
        'name':   'Saudi Arabia: Riyal',
        'ex': {
            'peso': 13.55,
            'usd': 0.26,
            'yaun': 1.71
        }
    },
    
    {
        'alias': 'peso',
        'name':   'Philippines: Peso',
        'ex': {
            'riyal': 0.47,
            'usd': 0.02,
            'yuan': 0.13,
        }
    },
    
    {
        'alias': 'usd',
        'name':   'USA: US Dollar',
        'ex': {
            'peso': 50.84,
            'riyal': 3.75,
            'yuan': 6.40
        }
    },
    {
        'alias': 'yuan',
        'name':   'China: Yuan',
        'ex': {
            'riyal': 0.59,
            'peso': 7.95,
            'usd': 0.16

        }
    }
];
module.exports = {currency};