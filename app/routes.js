const { exchangeRates } = require('../src/util.js');
// const {currency } =require('../data.js')



module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		let reqData = req.body;

		const aliasCheck = obj =>{
			return exchangeRates.hasOwnProperty(obj)
		}		
		const objectCheck = obj=> {
			for (var i in obj) return false;
			return true;
		}
		const trimCheck = (origin)=>{
			return typeof(origin) === 'string' ? origin.trim(): false
		}  
		const quickTrim = (origin) =>{
			return !(origin.trim())
		}
		
		
		//Validations
		if (
			//Property name check
			reqData.hasOwnProperty("name") 
			&& typeof(reqData.name) === 'string' 
			&& trimCheck(reqData.name)
			// checks if string, then trims string. if name is empty or is just spaces, returns falsey value
			
			//Property ex check
			&& reqData.hasOwnProperty("ex") 
			&& typeof(reqData.ex) ==='object'
			&& !objectCheck(reqData.ex)
			
			//Property alias check
			&& reqData.hasOwnProperty("alias") 
			&& typeof(reqData.alias) === 'string' 
			&& trimCheck(reqData.alias)
			){
				if (aliasCheck(reqData.alias)){
					return res.status(400).send({'Message':'Currency with alias already exists'})
				}

				return res.status(200).send();


			}
			else {
				if (!reqData.hasOwnProperty("name") ){
					return res.status(400).send({'Message':'Property_name_is_missing'});
				}
				else if (!(typeof(reqData.name) === 'string') ){
					return res.status(400).send({'Message':'Property_name_is_not_a_string'});
				}
				else if ( quickTrim(reqData.name) ){
					return res.status(400).send({'Message':'Property_name_is_empty_or_blank'});
				}
				else if ( !reqData.hasOwnProperty("ex")  ){
					return res.status(400).send({'Message':'Property_ex_is_missing'});
				}
				else if ( !(typeof(reqData.ex) ==='object') ){
					return res.status(400).send({'Message':'Property_ex_is_not_an_object'});
				}
				else if ( (objectCheck(reqData.ex)) ){
					return res.status(400).send({'Message':'Property_ex_is_an_empty_object'});
				}
				else if (!reqData.hasOwnProperty("alias") ){
					return res.status(400).send({'Message':'Property_alias_is_missing'});
				}
				else if (!(typeof(reqData.alias) === 'string') ){
					return res.status(400).send({'Message':'Property_alias_is_not_a_string'});
				}
				else if ( quickTrim(reqData.alias) ){
					return res.status(400).send({'Message':'Property_alias_is_empty_or_blank'});
				}
				
				
				
				
				
			}
			
			
			
			// if (reqData.name === null){
				// 	return res.status(400).send();
				// }



		//return status 200 if route is running
		// let data  = JSON.stringify(currency);
		// console.log(data);
		return res.status(200).send({
			
		})
	})
}
